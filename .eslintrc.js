/*
 * @Author: Yh
 * @LastEditors: Yh
 */
module.exports =  {
  "extends": [
    "react-app",
    "react-app/jest"
  ],
  "rules": {
    "eqeqeq": "error",
    "no-unused-vars": "error",
    "arrow-body-style": "warn",
    "react-hooks/rules-of-hooks": "warn",
  }
}