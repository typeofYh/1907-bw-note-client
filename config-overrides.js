/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const { override, addLessLoader, addWebpackAlias, adjustStyleLoaders } = require('customize-cra');
const path = require('path')

module.exports = {
  webpack: override(
    addLessLoader({
      lessOptions: {
        javascriptEnabled: true,
        modifyVars: { 
          '@primary-color': '#333',
          '@link-color': '#f81d22',
          '@font-size-base': '12px'
        },
      },
    }),
    adjustStyleLoaders(({ use: [, , postcss] }) => {
      const postcssOptions = postcss.options;  //postcss.options = {}
      postcss.options = { postcssOptions }; // postcss.options = {postcssOptions:{}}
    }),
    addWebpackAlias({
      '@': path.join(__dirname,'src'),
      'views': path.join(__dirname,'src/views'),
      'coms': path.join(__dirname,'src/components'),
    })
  )
}