/*
 * @Author: Yh
 * @LastEditors: Yh
 */

const context = require.context('./langs',true,/\.js$/); // 读取当前目录下所有的文件，生成当前目录上下文对象

// context 是一个函数
const langConfig = context.keys().reduce((value,filePath) => {
  // 第一次循环 value是{}
  // 第二次循环 value是 第一次循环的返回值
  // 加载上下文中的每一个文件
  const config = context(filePath).default; // 读文件内容
  const [,key] = filePath.match(/\.\/(\w+)\.js$/);
  if(!config){
    throw new Error(`langs "${filePath}" configFile not default export`)
  }
  return {
    ...value,
    [key]: {
      translation: config
    }
  }
}, {})


export default langConfig;

