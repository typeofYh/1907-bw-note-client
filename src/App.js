/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import Router from "./router";
import routerConfig from "@/router/router.config.js"
const App = () => (
  <Router routerConfig={routerConfig} />
);

export default App;