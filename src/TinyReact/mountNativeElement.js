/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import createDOMElement from './createDOMElement.js'
import {unmount} from "./utils"
export default function mountNativeElement (virtualDOM, container, oldDOM) {
    debugger
    // 加载原生元素
    const newElement = createDOMElement(virtualDOM)
    // 将转换之后的DOm对象放置在页面中
    if (oldDOM) {
        container.insertBefore(newElement, oldDOM)
    } else {
        container.appendChild(newElement)
    }
    // 如果旧的DOM对象存在 删除
    if (oldDOM) {
        unmount(oldDOM)
    }

    // 将 Virtual DOM 挂载到真实 DOM 对象的属性中 方便在对比时获取其 Virtual DOM
    newElement._virtualDOM = virtualDOM
    // 如果是组件渲染出来的 获取组件实例对象
    const component = virtualDOM.component
    // 如果组件实例对象存在
    if (component) {
        // 保存 DOM 对象
        component.setDOM(newElement)
    }
    
}