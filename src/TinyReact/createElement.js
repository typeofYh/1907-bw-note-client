// createElement 创建 VirtualDOM 对象
export default function createElement(type, props, ...children) {
  // children 是当前的子元素
  console.log(children)
  const childElements = [].concat(...children).reduce((result, child) => {
    if (child !== false && child !== true && child !== null) {
      if (child instanceof Object) { // 判断子元素是否是对象也就是元素节点
        result.push(child)
      } else { //子元素是文本节点
        result.push(createElement('text', { textContent: child}))
      }
    }
    return result
  }, [])
  return {
    type,
    props: Object.assign({children: childElements}, props), // props中应该包含children属性存储所有的子元素节点集合
    children: childElements
  }
}