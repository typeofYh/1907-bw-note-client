/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import mountElement from "./mountElement"
import updateElementNode from "./updateElementNode"

export default function createDOMElement(virtualDOM) {
  let newElement = null
 
  if (typeof virtualDOM === 'string' || (virtualDOM.type && virtualDOM.type === "text")) {
    // 创建文本节点
    newElement = document.createTextNode(typeof virtualDOM === 'string' ? virtualDOM : virtualDOM.props.textContent)
  } else {
    // 创建元素节点
    newElement = document.createElement(virtualDOM.type)
    // 更新元素属性
    updateElementNode(newElement, virtualDOM)
  }
  debugger
  // 递归渲染子节点
  virtualDOM.props && Array.isArray(virtualDOM.props.children) && virtualDOM.props.children.forEach(child => {
    // 因为不确定子元素是 NativeElement 还是 Component 所以调用 mountElement 方法进行确定
    mountElement(child, newElement)
  })
  if(virtualDOM.props && virtualDOM.props.children && typeof virtualDOM.props.children === 'string'){
    mountElement(virtualDOM.props.children, newElement)
  }

  if (virtualDOM.props && virtualDOM.props.ref) {
    virtualDOM.props.ref(newElement)
  }
  return newElement
}