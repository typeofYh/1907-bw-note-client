import mountNativeElement from './mountNativeElement.js'
import mountComponent from './mountComponent.js'
import {isFunction} from './utils'
export default function mountElement(virtualDOM, container, oldDOM) {
    // 在这里需要判断是原生元素节点还是 组件
    if (!isFunction(virtualDOM)) {
        // 通过调用 mountNativeElement 方法转换 Native Element
        mountNativeElement(virtualDOM, container, oldDOM)
    } else {
        // 如果是组件 调用 mountComponent 方法进行组件渲染
        mountComponent(virtualDOM, container, oldDOM)
    }
}