/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import axios from "@/utils/axios";
//推荐阅读
export const getRecommendList = () => axios.get('/api/article/recommend')
//文章标签
export const getTagList = (params) => axios.get('/api/tag', {
  params
})

//详情
export const getDetailData = (detailId) => axios.post(`/api/article/${detailId}/views`)

// 创建订单
export const createOrder = (data) => axios.post('/api/alipay/create', data)

// useState  useEffect useRef useReducer useCallback useMemo
// 所有的hooks只能在函数组件中使用
// 只能在函数组件根作用域下使用，不能写在if判断或for循环中

