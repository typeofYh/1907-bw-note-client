/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import axios from "@/utils/axios";

//评论列表
export const getCommentData = ({id,params}) => axios.get(`/api/comment/host/${id}`,{params})