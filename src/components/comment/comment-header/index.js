/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useState, useMemo } from "react"
import { Form, Input, Button } from "antd"
import { useParams, useLocation } from "react-router-dom"
const { TextArea } = Input;
const CommentHeader = ({isChildren = false, onClose = () => {}, placeholder = '请输入评论内容（支持 Markdown）', parentData = {} }) => {
  const [ submitting ] = useState(false)
  const { id } = useParams();
  const location = useLocation();
  const [ value, setvalue ] = useState('')
  const commentParamsData = useMemo(() => {
    const { email,name } = JSON.parse(localStorage.getItem('user'))
    let commonData = {
      content: value,
      email,
      hostId: id,
      name,
      url: location.pathname
    }
    if(isChildren) { // 子级评论
      commonData = {
        ...commonData,
        ...parentData
      }
    }
    return commonData
  }, [value, id, parentData, location]);
  const onChange = (e) => {
    setvalue(e.target.value);
  }
  const onSubmit = () => {
    console.log(commentParamsData);
    // 根据commentParamsData发送接口
  }
  return (
    <div>
      <Form.Item>
        <TextArea rows={4} onChange={onChange} value={value} placeholder={placeholder} />
      </Form.Item>
      <Form.Item>
        {
          isChildren && (
            <Button htmlType="button" onClick={onClose}>
              收起
            </Button>
          )
        }
        <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
          评论
        </Button>
      </Form.Item>
    </div>
  )
}

export default CommentHeader;