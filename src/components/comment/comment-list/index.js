/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Comment, Pagination } from 'antd';
import {useState, useEffect, useMemo} from "react"
import { useParams } from "react-router-dom"
import {getCommentData} from "@/api/comment"
import style from "./style.module.less"
import classname from "classname"
import { getNum, formatTime } from "@/utils/utils"
import CommenHeader from "../comment-header/"
const CommentItem = ({item}) => {
  const [headerOpen, setHeaderOpen] = useState(false);
  const handleChangeHeaderOpen = () => {
    setHeaderOpen(!headerOpen);
  }
  const backgroundColor = useMemo(() => `rgb(${getNum(0,255)},${getNum(0,255)},${getNum(0,255)})`, [])
  return (
    <Comment
      key={item.id}
      author={<span>{item.name}</span>}
      avatar={<span className={classname(style.author)} style={{
        backgroundColor
      }}>{item.name.slice(0,1)}</span>}
      content={
        <div>
          <b>{item.content}</b>
          <span onClick={handleChangeHeaderOpen}>回复</span>
          { headerOpen && <CommenHeader parentData={{parentCommentId:item.id,replyUserEmail:item.email,replyUserName:item.name}} isChildren={true} onClose={handleChangeHeaderOpen} placeholder={`回复 ${item.name}`} /> }
        </div>
      }
      datetime={
        <span>{item.userAgent} · {formatTime(item.updateAt)}</span>
      }
    >
      {
        Array.isArray(item.children) ? item.children.map(item => (<CommentItem item={item} key={item.id} />)) : null
      }
    </Comment>
  )
}
const CommentList = () => {
  const { id } = useParams();
  const [commentList, setCommentList] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageOption, setPageOption] = useState({
    page: 1,
    pageSize: 6
  })
  const init = async () => {
    const { data } = await getCommentData({
      id,
      params: pageOption
    })
    setCommentList(data[0])
    setTotal(data[1]);
  }
  useEffect(() => {
    // console.log('params',commentList,setCommentList);
    init();
  },[pageOption])

  const handleCommentListChange = (page, pageSize) => {
    setPageOption({
      page,
      pageSize
    })
  }
  return (
    <div>
      {
        commentList.map(item => (
          <CommentItem item={item} key={item.id} />
        ))
      }
      <Pagination 
        defaultCurrent={pageOption.page} 
        current={pageOption.page}
        total={total} 
        defaultPageSize={pageOption.pageSize}
        pageSize={pageOption.pageSize}
        onChange={handleCommentListChange}
      />
    </div>
  )
}

export default CommentList;