/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Fragment } from "react"
import CommentList from "./comment-list"
import CommentHeader from "./comment-header"

const Comment = () => (
    <Fragment>
      <CommentHeader></CommentHeader>
      <CommentList />
    </Fragment>
  )

export default Comment;