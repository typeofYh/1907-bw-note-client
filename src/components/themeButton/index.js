/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useState, useEffect } from "react"
import style from "./style.module.less"
import cls from "classname"

const ThemeButton = () => {
  const [theme, setTheme] = useState(true);
  // console.log(theme,setTheme);
  const handleChangeTheme = () => {
    setTheme(!theme);
  }
  useEffect(() => { // componentDidMount theme状态改变之后执行
    document.documentElement.className = '';
    document.documentElement.classList.add(theme ? 'light' : 'dark');
  }, [theme])
  return (
    <span className={cls(style.themeBtn,{
      [style.dark]: !theme
    })} onClick={handleChangeTheme}></span>
  )
}

export default ThemeButton;