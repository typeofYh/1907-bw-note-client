/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Card } from 'antd'
import { useEffect, useState, useCallback, useMemo } from "react"
import style from "./style.module.css"
const CommonList = ({title = '列表标题', getListApi, cardOption = {}, children}) => {
  const [list, setList] = useState([]);
  const init = useCallback(async () => {
    const data = await getListApi();  // 拦截器的作用，接口请求的结果改变
    if(data.code){
      setList(data.data)
    } else {
      // 走这
      setList({
        ...data
      })
    }
  }, [getListApi]);
  const loading = useMemo(() => {
    if(Array.isArray(list) && list.length){ // 数据获取到了 把loading隐藏 展示列表
      return false
    }
    if(list?.code === 0){ // list是一个对象 并且code属性为0 列表数据获取失败了  把loading隐藏 展示数据获取失败界面
      return false;
    }
    return true; // 列表正在加载中
  }, [list])
  useEffect(() => { // componentDidMount componentDidUpdate
    init();
  },[init]);
  const hanldeGetList = () => {
    init();
  }
  return <Card title={title} {...cardOption} loading={loading} style={{marginTop: '10px'}} className={style.mycard}>
    {
      Array.isArray(list) ? list.map((item) => children(item)) : <p onClick={hanldeGetList}>{list?.msg}</p>
    }
  </Card>
}

export default CommonList;