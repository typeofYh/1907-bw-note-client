/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useEffect, useState } from "react"
import { Dropdown, Menu, Button } from "antd"
import { useTranslation } from "react-i18next"
import langConfig from "@/i18n/config"

const LangButton = () => {
  const {t, i18n} = useTranslation();
  const [lang, setLang] = useState('en');
  const hanldChangeLang = ({ key }) => {
    setLang(key);
  }
  useEffect(() => {
    i18n.changeLanguage(lang);  // 切换语言
  }, [lang])
  const menu = (
    <Menu onClick={hanldChangeLang}>
      {
        Object.keys(langConfig).map(item => (
          <Menu.Item key={item}>
            {t(item)}
          </Menu.Item>
        ))
      }
    </Menu>
  );
  return (
    <Dropdown overlay={menu} placement="bottomLeft">
      <Button>{t('lang')}</Button>
    </Dropdown>
  )
}

export default LangButton;