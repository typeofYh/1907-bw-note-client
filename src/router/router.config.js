/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { lazy } from "react";
export const LayoutChildren = [
  {
    path: 'home',
    meta: {
      title: 'Articles',
      isNav: true
    },
    component: lazy(() => import('@/views/article'))
  },
  {
    path: 'archives',
    meta: {
      title: 'Archives',
      isNav: true
    },
    component: lazy(() => import('@/views/archives'))
  },
  {
    path: 'knowledge',
    meta: {
      title: 'Knowledge Books',
      isNav: true
    },
    component: lazy(() => import('@/views/knowledge'))
  },
  {
    path: 'article/:id',
    meta: {
      title: '八维创作平台',
      isNav: false
    },
    component: lazy(() => import('@/views/detail'))
  }
]
const routes = [
  {
    path: '/',
    meta: {
      title: '八维创作平台',
    },
    component: lazy(() => import('@/views/layout')),
    children: LayoutChildren
  },
  {
    path: '*',
    component: lazy(() => import('@/views/notFind'))
  }
]

export default routes;