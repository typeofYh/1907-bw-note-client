/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Routes, Route, HashRouter, BrowserRouter } from "react-router-dom"
import { Suspense } from "react"
import { useEffect } from "react"
import NProgress from "nprogress"
import { useTranslation } from "react-i18next"
import "nprogress/nprogress.css"
// console.log(NProgress);
const BeforeRouter = ({component:ViewCom, meta}) => {
  const { t } = useTranslation();
  NProgress.start();
  useEffect(() => {
    document.title = t(meta?.title);
    NProgress.done()
  },[meta])
  return <ViewCom />
}
const Loading = () => {
  NProgress.start();
  return <div>
    loading...
  </div>
}
export const RouterView = ({routerConfig = []}) => {
  const renderChildren = (routeArr) => {
    if(routeArr.length){
      return routeArr.map(({path, component:ViewCom, children = [], meta = {}}) => (
        <Route key={path} path={path} element={<BeforeRouter component={ViewCom} meta={meta} />}>
          {
            renderChildren(children)
          }
        </Route>
      ))
    }
    return [];
  }
  return (
    <Suspense fallback={<Loading />}>
      <Routes>
        {
          renderChildren(routerConfig)
        }
      </Routes>
    </Suspense>
  )
}

const Router = ({routerConfig = [],mode = "history"}) => {
  if(mode === 'history'){
    return (
      <BrowserRouter>
        <RouterView routerConfig={routerConfig} />
      </BrowserRouter>
    )
  }
  return (
    <HashRouter>
        <RouterView routerConfig={routerConfig} />
    </HashRouter>
  ) 
}


export default Router;