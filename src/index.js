/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import React from "react"
import {render} from "react-dom"
import App from "./App"
import "@/style/common.less"
import "@/style/theme.css"
import "@/i18n"



render(<App />, document.getElementById('root'));

