/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import CommonList from "coms/commonList"
import {getRecommendList, getTagList} from "@/api/article"
const Article = () => (
  <div className="content-wrappper">
    <div className="left-content">
      <div>
        文章内容
      </div>
    </div>
    <div className="right-content">
      <CommonList 
        title="推荐阅读"
        getListApi={getRecommendList}
      >
        {
          (item) => (
            <p key={item.id}>
              <span>{item.title}</span> · <span>{item.publishAt}</span>
            </p>
          )
        }
      </CommonList>
      <CommonList 
        title="文章标签" 
        getListApi={getTagList}
      >
        {
          (item) => (
            <p key={item.id}>
              {item.label}[{item.articleCount}]
            </p>
          )
        }
      </CommonList>
    </div>
  </div>
)

export default Article;