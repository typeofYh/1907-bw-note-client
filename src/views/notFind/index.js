/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useState, useEffect, useRef } from "react"
import { useNavigate } from "react-router-dom"
const NotFind = () => {
  const [time, setTime] = useState(3);  
  const timer = useRef(null); // 值不会跟着组件状态更新会保留用户存储的值
  const navigate = useNavigate();
  const handleBackHome = () => navigate('/');
  // useState定义函数式组件的状态 接受状态初始值 返回值是一个数组[当前状态, 设置状态的函数]
  // useEffect 模拟特定时间内执行的操作
  // 第二个参数如果是空数组 等价于componentDidMount 只在函数创建的时候执行一次
  // 第二个参数不存在 等价于componentDidMount + componentDidUpdate 在函数创建和更新（只要调用useState的更新函数）时都会执行
  // useEffect 第一个参数的返回函数 等价于 componentWillUnment 组件卸载之前执行
  useEffect(() => {
    clearInterval(timer.current);
    timer.current = setInterval(() => {
      timer.current && setTime((time) => time - 1);
    },1000)
    return () => { // 组件卸载之前执行
      clearInterval(timer.current);
      timer.current = null;
    }
  }, [timer])
  useEffect(() => {
    if(time < 1){
      timer.current = null;
      navigate('/');
    }
  },[time, navigate])
  return (
    <div>
      页面走丢啦~ 请点击按钮<button onClick={handleBackHome}>返回首页</button>，或{time}秒之后自动跳转首页
    </div>
  )
}

export default NotFind;