/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Component } from "react"

class NotFind extends Component {
  state = {
    time: 3
  }
  componentDidMount(){
    this.timer = setInterval(() => {
      this.setState({
        time: this.state.time - 1
      },() => {
        if(this.state.time < 1){
          console.log('回到首页销毁组件');
        }
      })
    },1000)
  }
  handleBackHome = () => {
    console.log('回到首页销毁组件');
  }
  componentWillUnmount(){
    console.log('componentWillUnmount');
    clearInterval(this.timer);
    this.timer = null;
  }
  render(){
    const { handleBackHome } = this;
    const { time } = this.state;
    return (
      <div>
        页面走丢啦~ 请点击按钮<button onClick={handleBackHome}>返回首页</button>，或{time}秒之后自动跳转首页
      </div>
    )
  }
}
export default NotFind;