/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { NavLink, Outlet } from "react-router-dom"
import { Button } from "antd"
import ThemeButton from "coms/themeButton"
import { LayoutChildren } from "@/router/router.config"
import { useMemo } from "react"
import { useTranslation } from "react-i18next"
import LangButton from "coms/langButton"
const Layout = () => {
  const { t } = useTranslation();
  const navData = useMemo(() => LayoutChildren.filter(val => val?.meta?.isNav).map(item => ({
      path: '/' + item.path,
      title: item?.meta?.title
    })),[])
  return (
    <div className="wrapper">
      <header className="header">
        <div className="container">
          <div className="logo"></div>
          <ul className="header-nav">
            {
              navData.map(item => (
                <li key={item.path}>
                  <NavLink to={item.path}>{t(item.title)}</NavLink>
                </li>
              ))
            }
            <li><ThemeButton /></li>
            <li><LangButton /></li>
          </ul>
          <Button className={'menu'}>菜单</Button>
        </div>
      </header>
      <section className="content">
        <div className="container">
          <Outlet />
        </div>
      </section>
      <footer className="footer">
        <div className="container">
          footer<Button type="link">1111</Button>
        </div>
      </footer>
    </div>
  )
}

export default Layout;