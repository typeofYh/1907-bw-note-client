/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useParams, useNavigate } from "react-router-dom"
import { notification, message, Modal } from "antd"
import { useEffect, useCallback, useState, useRef } from "react"
import { getDetailData, createOrder, getRecommendList } from "@/api/article"
import CommonList from "coms/commonList"
import copy from "copy-to-clipboard";
import Comment from "coms/comment"
// import MarkdownIt from "markdown-it"
// import hljs from "highlight.js"
import "@/style/markdown.less"


const Detail = () => {
  const { id } = useParams(); // 获取详情id
  const [detailData, setDetailData] = useState(); // 详情数据
  const [payAlert, setPayAlert] = useState({
    open: false
  });
  const Navigate = useNavigate(); // 路由
  const [content, setContent] = useState(''); // 详情内容
  const contentDom = useRef(null);
  // 格式化html 添加copy按钮
  const formatterHTMl = useCallback((html) => {
    html = html.replace(/<pre>\s?<code>/g, `<pre><span class="copy-btn">复制</span><code>`)
    return html;
  }, []);
  // 初始获取数据
  const init = useCallback(async () => {
    const { data } = await getDetailData(id)
    if(data.totalAmount){
      setPayAlert({
        open: true,
        totalAmount: data.totalAmount 
      })
    }
    setDetailData(data);
    setContent(formatterHTMl(data.html));
  }, [id])

  const addCopyEvent = () => {
    contentDom.current.addEventListener('click' , contentDom.current.eventFn = (e) => {
      // 事件委托
      if(e.target.classList.contains('copy-btn')){
        //点击copy按钮
        copy(e.target.nextElementSibling.innerHTML, {
          message: '复制成功',
        });
        message.success('复制成功');
      }
    })
  }

  const removeCopyEvent = () => {
    contentDom.current.removeEventListener('click', contentDom.current.eventFn);
  }
  // componentDidMount
  useEffect(() => {
    // 获取数据
    init();
    contentDom.current && addCopyEvent();

    return () => {
      contentDom.current && removeCopyEvent();
    }
  },[init])

  if(!id){
    notification.error({
      message: '您的链接不合法',
      onClose: () => {
        Navigate('/home')
      }
    })
    return null;
  }
  // 立即支付按钮点击事件
  const handleOk = async () => {
    const data = await createOrder({
      id,
      totalAmount: payAlert.totalAmount
    })
    // console.log(data);
    window.location.href = data.data
  }
  const handleCancel = () => {
    Navigate('/home')
  }
  return (
    <div className="content-wrappper">
      <Modal title="确认以下收费信息" 
        visible={payAlert.open} 
        onOk={handleOk} 
        onCancel={handleCancel} 
        cancelText={'取消'} 
        okText={"立即支付"}
      >
        <p>支付金额：{payAlert.totalAmount}</p>
      </Modal>
      <div className="left-content">
        <div className="detail-content-wrapper">
          <div className="detail-images">
            <img src={detailData?.cover} />
          </div>
          <div className="detail-content markdown" ref={contentDom} dangerouslySetInnerHTML={{__html: content}}></div>
        </div>
        <div className="detail-comment">
          <h2>评论</h2>
          <Comment />
        </div>
      </div>
      <div className="right-content">
        <CommonList 
          title="推荐阅读"
          getListApi={getRecommendList}
        >
          {
            (item) => (
              <p key={item.id}>
                <span>{item.title}</span> · <span>{item.publishAt}</span>
              </p>
            )
          }
        </CommonList>
      </div>
    </div>
  )
}

export default Detail;