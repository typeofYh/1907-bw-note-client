/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import "dayjs/locale/zh-cn"
dayjs.extend(relativeTime)
dayjs.locale('zh-cn')
export const getNum = (min,max) => Math.floor(Math.random() * (max - min) + 1);

export const formatTime = (createTime) => {
  const curTime = dayjs()
  const targetTime = dayjs(createTime)
  return curTime.from(targetTime)
}