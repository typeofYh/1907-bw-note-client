/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import axios from "axios";
import { message } from "antd"
// todo axios 二次封装
const httpTool = axios.create({
  timeout: 10000
})

httpTool.interceptors.response.use(({data}) => {
  // 接口返回成功状态的时候执行
  console.log('拦截器:success:',data);
  return Promise.resolve({
    code: 1,
    ...data
  })
},(error) => {
  let msg = '';
  if(error.code === 'ECONNABORTED'){
    msg = '网络请求超时，请刷新重试~'
    message.error(msg);
    return Promise.resolve({
      code:0,
      msg
    })
  }
})
export default httpTool;