<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
### 八维创作平台

1. 项目基础建设

在进行项目开发之前，尤其是多人协作的项目，需要3个层面
约定好代码规范(语法规范eslint+代码风格preitter)
提交规范(跟git配合校验我们的代码提交 husky)
线上问题异常监控（sentry）

```js
const arr = [1,2,4,5];
arr.forEach(item => {
  console.log(item + 1)
})
arr.map(item => {
  return item + 1
})

const a = 'a'
console.log(a);
function run() {
  console.log('run')
}
run()
```

> eslint
> 可组装的JavaScript和JSX检查工具(检查js代码代码语句规范)
-  支持JavaScript、JSON .eslintrc.js/.eslintrc.json 根目录下的
```js
module.exports =  {
  "extends": [  //扩展语法包
    "react-app",
    "react-app/jest"
  ],
  "rules": {  // 自定义规则
    "eqeqeq": "error",
    "no-unused-vars": "error"
  }
  // env 运行环境
}
```

> prettier 约定代码风格 可以结合eslint去使用

- git commit 之前检测代码是否规范

- 执行git commit 的时候可以监听hooks钩子函数

1. 下载`husky`
2. 在`package.json`中`scripts`添加`husky install`指令，目录中会多一个`.husky`文件夹
3. 添加钩子函数，在命令行执行`npx husky add .husky/pre-commit "npm run lint"` 执行之后.husky文件夹中会多一个pre-commit的文件
4. 在`package.json`中`scripts`添加`lint`指令`eslint --fix --ext \".js,.jsx,src\/\" --ignore-path .gitignore .`，指令内容去运行eslint语法检测


#### 什么是jsx
> jsx是`react.createElement`的语法糖，需要通过babel转译解析

#### 什么是虚拟dom
> 描述dom节点的js对象


#### 主题切换
1. 通过css变量实现主题切换（*） 不断切换document.doucmentElement的类名切换css变量值
```css
/*定义变量*/
:root.drak {
  --var-name:变量值;
} /*html元素，js通过document.doucmentElement*/

/*使用变量*/
.className {
  background:var(--var-name);
}
```

2. 通过动态编译less 修改less变量值实现主题切换


#### css module
1. css 模块化 ，组件样式互相不影响，给当前组件元素添加属性选择器，或者当前组件元素类名有自己的哈希值

#### 响应式布局
- 用一套样式和结构实现多端
768以下的屏幕  container的宽度是100% （推荐阅读和文章文类隐藏） 导航变化：按钮显示，导航列表定位出现到下拉菜单
768 - 992 的屏幕  container的宽度是768  导航变化：按钮隐藏，导航列表不定位
992 以上 container的宽度是970  导航变化：按钮隐藏，导航列表不定位


#### react 性能优化

1. useCallback
- 作用： 返回缓存之后的函数
- 使用方法: 跟useEffect的使用方法一样，第一参数是一个函数，第二参数是选填参数，可以不写，可以写一个数组，数组中存放属性，只要该属性发生变化，useCallback 返回新函数
```js
const run = useCallback(() => {

})

// run是一个新函数
```

#### 真机测试
1. 保证手机和电脑链接同一个网络
2. windows把电脑防火墙关闭
3. 在电脑打开ipv4的地址

document.title

#### merge request



#### react 生命周期

